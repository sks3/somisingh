---
title: Week 11
date: 2018-11-11
---
![Somi Singh](https://raw.githubusercontent.com/sks3/tipster/master/TipsterUITests/me.jpg "Me")

**What did you do this past week?**

This week I finished up the interviews I was planning on doing.  Some of them
were in Austin and others required traveling, so I spent that time working on 
programming assignments and getting caught up on readings.

**What's in your way?**

I am still working on getting caught up on class the past few weeks, while
also working on finishing the Darwin project.  Other than getting caught up,
nothing is in my way for the rest of the year.

**What will you do next week?**

Next week I will finish the Darwin project and continue getting caught up in this
class.  I will also start to look at the materials for next semester before 
registration is over for the year, in case I want to switch my schedule at the last
minute like I typically do.

**You have read five papers that describe SOLID design : Single responsibility, 
Open-closed principle, Liskov substitution, Interface segregation, Dependency inversion. 
What insights have they given you?**

Having read these during Software Engineering, reading over these papers again
was a good reminder that commonly held beliefs about object-oriented programming
can be taken different ways.  I have learned that in some situations, it is not
advisable to use setters or getters, and that using templates and generics 
wherever possible allows for future developers to change the implementations
without having an adverse effect on the rest of the project.

**What's your pick-of-the-week or tip-of-the-week?**

My pick of the week is Elements of Programming Interviews (EPI).  This book is
an alternative or complement to Cracking the Coding Interview, and is co-authored
by an ECE professor here at UT.  It is currently offered in Python, Java and C++.

https://elementsofprogramminginterviews.com/

https://www.amazon.com/Elements-Programming-Interviews-Insiders-Guide/dp/1479274836



